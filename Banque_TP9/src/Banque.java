import java.util.LinkedList;

public class Banque {
	
	private String nom;
	private LinkedList<CompteBancaire> compte;
	
	public Banque(String n) {
		this.nom = n;
		this.compte = new LinkedList<>();
	}
	
	public String getLibell�() {
		return this.nom;
	}
	
	public CompteBancaire ouvrir(String num, Float v)throws IllegalArgumentException {
		if (v < 0 || estCompteExistant(num)) {
			throw new IllegalArgumentException();
		}
		CompteBancaire cb = new CompteBancaire(num);
		cb.d�poser(v);
		compte.add(cb);
		return cb;	
	}
	
	public void fermer(String num) throws IllegalArgumentException{
		CompteBancaire cb = getCompte(num);
		if(!estCompteExistant(num) || cb.getSolde() != 0){
			throw new IllegalArgumentException();
		}
			
		compte.remove(cb);
	}
	
	public Float getSoldeBanque() {
		Float s = 0.0F;
		for(CompteBancaire c : this.compte) {
			s+= c.getSolde();
		}
		return s;
	}
	
	
	
	public CompteBancaire getCompte(String num) {
		for(CompteBancaire c : this.compte) {
			if(c.getNum�ro().equals(num)) {
				return c;
			}
		}
		return null;
		
	}
	
	public void d�poser(String num, Float v)throws IllegalArgumentException {
		if(!estCompteExistant(num) || v < 0) {
			throw new IllegalArgumentException();
		}
		getCompte(num).d�poser(v);
		
	}
	
	public void retirer(String num, Float v)throws IllegalArgumentException {
		if(!estCompteExistant(num) || v < 0) {
			throw new IllegalArgumentException();
		}
		getCompte(num).retirer(v);
		
	}
	
	public Boolean estCompteExistant(String num) {
		
		for(int i=0; i < compte.size(); i++) {
			if (this.compte.get(i).getNum�ro() == num) {
				return true;
			}
		}
		return false;
	}
	
	public void affichercomptesD�biteurs() {
		String s =  "Comptes ayant un solde n�gatif :" ;
		for(CompteBancaire c : this.compte) {
			if(c.getSolde() < 0) {
				s+="\n"+c.toString();
			}
		}
		System.out.println(s);
	}
	
	@Override
	public String toString() {
		String s;
		s = "[Banque : "+this.getLibell�();
		for(CompteBancaire c : this.compte) {
			s+="\n"+c.toString();
		}
		s += "]";
	    return s;
	}
	
}
