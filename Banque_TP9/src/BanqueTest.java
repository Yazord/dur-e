import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BanqueTest {
	
	Banque b;

	@Before
	public void setUp() {
		this.b = new Banque("Caisse");
		this.b.ouvrir("1234", 50.0F);
		this.b.ouvrir("9876", 10.0F);
	}
	
	@After
	public void tearDown() {
		this.b = null;
	}

	@Test
	public void testGetLibell�() {
		assertEquals("Caisse",this.b.getLibell�());
	}

	@Test
	public void testOuvrir() {
		assertTrue(this.b.estCompteExistant("1234"));
	}

	@Test
	public void testFermer() {
		this.b.retirer("1234", 50.0F);
		this.b.fermer("1234");
		assertFalse(this.b.estCompteExistant("1234"));
	}

	@Test
	public void testGetSoldeBanque() {
		assertEquals(this.b.getSoldeBanque(), 60.0F, 0.0F);
	}

	@Test
	public void testGetCompte() {
		assertTrue(this.b.getCompte("1234").equals(new CompteBancaire("1234")));
	}

	@Test
	public void testD�poser() {
		this.b.d�poser("1234", 10.0F);
		assertEquals(this.b.getCompte("1234").getSolde(), 60.0F, 0.0F);
	}

	@Test
	public void testRetirer() {
		this.b.retirer("1234", 10.0F);
		assertEquals(this.b.getCompte("1234").getSolde(), 40.0F,0.0F);
	}

	@Test
	public void testEstCompteExistant() {
		assertTrue(this.b.estCompteExistant("1234"));
		assertFalse(this.b.estCompteExistant("2345"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionOuvrirCompteExistant() {
		this.b.ouvrir("1234", 50.0F);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionOuvrirArgentNegatif() {
		this.b.ouvrir("001", -50.0F);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionFermeCompteInexistant() {
		this.b.fermer("001");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionFermeArgentNonNul() {
		this.b.fermer("1234");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionDeposerCompteInexistant() {
		this.b.d�poser("001",10.0F);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionDeposerArgentNegatif() {
		this.b.d�poser("1234", -20.0F);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionRetirerCompteInexistant() {
		this.b.retirer("001", 20.0F);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionRetirerArgentNegatif() {
		this.b.retirer("1234", -20.0F);
	}
	
	
	@Test
	public void testToString() {
		this.b.retirer("9876", 10.0F);
		this.b.fermer("9876");
		assertEquals("[Banque : Caisse\nNum�ro : 1234, Cr�dit : 50.0, D�bit : 0.0]",this.b.toString());
	}
	
}
