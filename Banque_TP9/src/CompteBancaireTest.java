import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import org.junit.Test;

public class CompteBancaireTest {

	@Test
	public void TestEquals_egaliteObjet() {
		CompteBancaire c1 = new CompteBancaire("1234");
		assertSame(c1,c1);
		
	}
	
	@Test
	public void TestEquals_egaliteNumero() {
		CompteBancaire c1 = new CompteBancaire("1234");
		CompteBancaire c2 = new CompteBancaire("1234");
		assertEquals(c1,c2);
		
	}
	
	@Test
	public void TestEquals_ObjectNull() {
		CompteBancaire c1 = null;
		assertNull(c1);
		
	}
	
	@Test
	public void TestEquals_NumeroNull() {
		CompteBancaire c1 = new CompteBancaire("");
		CompteBancaire c2 = new CompteBancaire("1234");
		assertNotEquals(c1,c2);
		
	}
	
}
