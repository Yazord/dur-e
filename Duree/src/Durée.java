
public class Dur�e {
	
	private final int heure;
	private final int minute;
	private final int seconde;
	
	
	//Construit une duree depuis une heure, une minute et une seconde
	//renvoie une exception si h<0 ou 60 >= m >0 ou 60 >= m > 0 
	public Dur�e(int h, int m, int s ) throws IllegalArgumentException {
		if (h < 0 || 60 <= m || m < 0 || 60 <= s || s < 0 ) {
			throw new IllegalArgumentException("Erreur dans la saisie de l'heure");
		}
		this.heure = h;
		this.minute = m;
		this.seconde = s;
	}
	
	//retourne l'heure d'une dur�e
	public int getHeures() {
		return this.heure;
	}
	
	//retourne les minutes d'une dur�e
	public int getMinutes() {
		return this.minute;
		
	}
	
	//retourne les secondes d'une dur�e
	public int getSecondes() {
		return this.seconde;
	}
	
	//retourne vrai si les deux dur�e sont egales
	public boolean �gal(Dur�e d) {
		return this.heure == d.heure && this.minute == d.minute && this.seconde == d.seconde;
	}
	
	//retourne vrai si la dur�e est inferieur a l'autre
	public boolean inf(Dur�e d) {
		return (this.heure < d.heure) || (this.heure == d.heure && this.minute < d.minute) || (this.heure == d.heure && this.minute == d.minute && this.seconde < d.seconde);
	}
	
	//ajoute un a une dur�e
	public Dur�e ajouterUneSeconde() {
		int h = this.heure;
		int m = this.minute;
		int s = this.seconde;
		if (s < 59) {
			s++;
		}
		else if(m < 59) {
				m++;
				s = 0;
			
		}
		else {
			  s = 0;
			  m = 0;
			  h++;
		}
		return new Dur�e(h,m,s);
		
	}
	
	@Override
	public String toString() {
		return this.heure+":"+this.minute+":"+this.seconde;
		
	}
}
