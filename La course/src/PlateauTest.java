import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PlateauTest {

	
	@Test
	public void testCaseNormale() {
		Plateau p = new Plateau();
		assertEquals(p.calculDeplacement(0, 3),3);
		assertEquals(p.calculDeplacement(47, 3),-1);
	}
	
	@Test
	public void testCaseAttente() {
		Plateau p = new Plateau();
		assertEquals(p.calculDeplacement(12, 3),0);
		assertEquals(p.calculDeplacement(12, 2),2);
		assertEquals(p.calculDeplacement(18, 3),0);
		assertEquals(p.calculDeplacement(18, 5),5);
		assertEquals(p.calculDeplacement(24, 3),0);
		assertEquals(p.calculDeplacement(24, 2),2);
		assertEquals(p.calculDeplacement(27, 3),0);
		assertEquals(p.calculDeplacement(27, 5),5);
		assertEquals(p.calculDeplacement(33, 3),0);
		assertEquals(p.calculDeplacement(33, 4),4);
		
	}

}
