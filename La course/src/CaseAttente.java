
public class CaseAttente extends Case {
	
	private int valeurAttendue;
	
	public CaseAttente(int numero, int valeurAttendue) {
		super(numero);
		this.valeurAttendue = valeurAttendue;
	}

	
	public int calculDeplacement(int valeur) {
		if (valeur == this.valeurAttendue) {
			return this.valeurAttendue;
		}
		else return 0;
	}
	
	
	@Override
	public String toString() {
		return  super.toString()+" avancer la valeur du d� si elle est egale � "+this.valeurAttendue;
	}

}


	