
public class Plateau {
	
	public static final int NB_CASES = 49;
	private Case[] c;
	
	public Plateau() {
		this.c = new Case[Plateau.NB_CASES];
		for (int i = 0; i < NB_CASES; i++) {
			switch(i) {
			case 12: c[i] = new CaseAttente(i,2);
					  break;
			case 18: c[i] = new CaseAttente(i,5);
			 	     break;
			case 24:  c[i] = new CaseAttente(i,2);
			          break;
			case 27:  c[i] = new CaseAttente(i,5);
			          break;
			case 33:  c[i] = new CaseAttente(i,4);
			          break;
			default : c[i] = new CaseNormale(i);
			}
		}
	}
	
	public int calculDeplacement(int position, int valeur) {
			int dep = this.c[position].calculDeplacement(valeur);
			System.out.println(c[position]);
			if (dep + position <= Plateau.NB_CASES - 1) {
				return dep;
			}
			else return 2*(Plateau.NB_CASES - 1 - position) - dep;
	}

}
	