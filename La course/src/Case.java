
public abstract class Case {
	
	protected int numero;

	public Case(int numero) {
		this.numero = numero;
	}
	
	public abstract int calculDeplacement(int valeur);
	
	@Override
	public String toString() {
		return "[case n�"+this.numero+"]";
	}
	
}
