
public class Jeu {
	
	private int position;
	private Plateau p;
	
	public Jeu() {
		this.position = 0;
		this.p = new Plateau();
	}
	
	public void jouerPartie() {
		while (!estPartieFinie()){
			System.out.println("Position avant : " + this.position);
			jouerPion();
			System.out.println("Position apr�s : " + this.position);
			System.out.println("");
		}
		
	}
	
	public Boolean estPartieFinie() {
		return this.position == Plateau.NB_CASES - 1;
		
	}
	
	public void jouerPion() {
		this.position +=  this.p.calculDeplacement(this.position, this.lancerDe());
	}
	
	public int lancerDe() {
		int de = (int) (6.0 * Math.random() + 1.0);
		System.out.println("lancer du d� : "+ de);
		return de;
	}
	
	

}
