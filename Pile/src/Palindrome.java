import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez un mot");
		String mot = sc.nextLine();
		PileBornee<Character> p1 = new PileBornee<Character>(mot.length());
		for(int i=0; i < p1.getMaxSize(); i++) {
			try {
				p1.push(mot.charAt(i));
			}catch(FullStackException e) {
				System.out.println(e);
			}
		}
		String inv = "";
		for(int i=0; i < p1.getMaxSize(); i++) {
			try {
			inv += p1.top();
			p1.pop();
			}catch(EmptyStackException e) {
				System.out.println(e);
			}
		}
		
		boolean pal = true;
		for(int i=0; i < p1.getMaxSize(); i++) {
			if (inv.charAt(i) != mot.charAt(i)) {
				pal = false;
			}
		}
		System.out.println(pal);
		sc.close();
	}

}
