import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PileTest {
	
	private final int MAX_SIZE=2;
	private PileBornee<Integer> p;
	
	@Before
	public void setUp() {
		p = new PileBornee<Integer>(MAX_SIZE);
	}
	
	@After
	public void tearDown() {
		p = null;
	}
	
	@Test
	public void testNouvellePileVide() {
		assertTrue(this.p.isEmpty());
	}
	
	@Test
	public void testPush() throws FullStackException {
		this.p.push(3);
		assertFalse(this.p.isEmpty());
		
	}
	
	@Test
	public void testMaxSize() {
		assertEquals(this.p.getMaxSize(),MAX_SIZE);
	}
	
	@Test
	public void testMaxSizeApresPush() throws FullStackException {
		p.push(5);
		assertEquals(this.p.getMaxSize(),MAX_SIZE);
	}
	
	@Test
	public void testNombreElementNouvellePile() {
		assertEquals(this.p.getSize(), 0);
	}
	
	@Test 
	public void testNombreElementApresUnPush1() throws FullStackException {
		this.p.push(5);
		assertEquals(this.p.getSize(),1);
		this.p.push(5);
		assertEquals(this.p.getSize(),2);
	}
	
	@Test
	public void testIsFullPileVide() {
		assertFalse(this.p.isFull());
	}

	@Test
	public void testIsFull() throws FullStackException {
		this.p.push(2);
		assertFalse(this.p.isFull());
		this.p.push(2);
		assertTrue(this.p.isFull());
	}
	
	@Test 
	public void testTop()throws EmptyStackException, FullStackException {
		this.p.push(5);
		assertEquals((int)this.p.top(),5);
		this.p.push(2);
		assertEquals((int)this.p.top(),2);
	}
	
	
		@Test
	public void testPop()throws EmptyStackException, FullStackException {
		this.p.push(2);
		this.p.pop();
		assertTrue(this.p.isEmpty());
		this.p.push(2);
		this.p.push(1);
		this.p.pop();
		assertEquals((int)this.p.top(),2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionConstructeur()throws IllegalArgumentException {
		p = new PileBornee<Integer>(-2);
	}
	
	@Test(expected = EmptyStackException.class)
	public void testExceptionTop() throws EmptyStackException {
		int n = this.p.top();
	}
	
	@Test(expected = FullStackException.class)
	public void testExceptionPush() throws FullStackException{
		PileBornee<Integer> p1 = new PileBornee<Integer>(1);
		p1.push(2);
		p1.push(3);
	}

}
