import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestPolynome {

    @Test
    public void testtoStringPolynomeNul() {
        Polynome nul = new Polynome();
        assertEquals("0", nul.toString());
    }

    @Test
    public void testtoStringPolynomeCoefficientsPositifs() {
        Polynome p = new Polynome();
        p.setMonome(new Monome(8F, 4));
        p.setMonome(new Monome(8F, 2));
        p.setMonome(new Monome(1F, 0));
        assertEquals("8.0xe4 + 8.0xe2 + 1.0", p.toString());
    }

    @Test
    public void testtoStringPolynomeCoefficientsPositifsEtN�gatifs() {
        Polynome p = new Polynome();
        p.setMonome(new Monome(128F, 8));
        p.setMonome(new Monome(160F, 4));
        p.setMonome(new Monome(-32F, 2));
        p.setMonome(new Monome(1F, 0));
        assertEquals("128.0xe8 + 160.0xe4 - 32.0xe2 + 1.0", p.toString());
    }

    @Test
    public void testtoStringPolynomePremierCoefficientN�gatif() {
        Polynome p = new Polynome();
        p.setMonome(new Monome(-128F, 8));
        p.setMonome(new Monome(160F, 4));
        p.setMonome(new Monome(-32F, 2));
        p.setMonome(new Monome(1F, 0));
        assertEquals("-128.0xe8 + 160.0xe4 - 32.0xe2 + 1.0", p.toString());
    }

    @Test
    public void testDerivePolynome() {
        Polynome p = new Polynome();
        p.setMonome(new Monome(2F, 2));
        p.setMonome(new Monome(6F, 1));
        p.setMonome(new Monome(32F, 0));
        assertEquals("4.0x + 6.0", p.d�riv�e().toString());
    }

    @Test
    public void testSommePolynomes() {
        Polynome p = new Polynome();
        p.setMonome(new Monome(-128F, 8));
        p.setMonome(new Monome(160F, 4));
        p.setMonome(new Monome(-32F, 2));
        p.setMonome(new Monome(1F, 0));
        Polynome other = new Polynome();
        other.setMonome(new Monome(128F, 8));
        other.setMonome(new Monome(6F, 4));
        other.setMonome(new Monome(33F, 3));
        assertEquals("166.0xe4 + 33.0xe3 - 32.0xe2 + 1.0",
                p.somme(other).toString());
    }

    @Test
    public void testProduitPolynomeParMonome() {
        Polynome p = new Polynome();
        p.setMonome(new Monome(128F, 8));
        p.setMonome(new Monome(160F, 4));
        p.setMonome(new Monome(-32F, 2));
        p.setMonome(new Monome(1F, 0));
        Monome m = new Monome(2F, 1);
        assertEquals("256.0xe9 + 320.0xe5 - 64.0xe3 + 2.0x",
                p.produitMonome(m).toString());
    }
    
    @Test
    public void testProduitPolynomeParPolynome() {
        Polynome p = new Polynome();
        p.setMonome(new Monome(-3.0F, 2));
        p.setMonome(new Monome(4.0F, 1));
        p.setMonome(new Monome(-2.0F, 0));
        Polynome other = new Polynome();
        p.setMonome(new Monome(1.0F, 3));
        p.setMonome(new Monome(-1.0F, 1));
        p.setMonome(new Monome(1.0F, 0));
        assertEquals("- 3.0ex5 + 4.0xe4 + 1.0xe3 - 7xe2 + 6x - 2",
                p.produit(other).toString());
    }
    
    @Test
    public void testGetDegres() {
    	 Polynome p = new Polynome();
         p.setMonome(new Monome(-128F, 8));
         p.setMonome(new Monome(160F, 4));
         p.setMonome(new Monome(-32F, 2));
         p.setMonome(new Monome(1F, 0));
         Polynome nul = new Polynome();
         assertEquals(p.getDegr�s(),8);
         assertEquals(nul.getDegr�s(),0);
    	
    }
    
    @Test
    public void testDiffrence() {
    	  Polynome p = new Polynome();
          p.setMonome(new Monome(-128F, 8));
          p.setMonome(new Monome(160F, 4));
          p.setMonome(new Monome(-32F, 2));
          p.setMonome(new Monome(1F, 0));
          Polynome other = new Polynome();
          other.setMonome(new Monome(128F, 8));
          other.setMonome(new Monome(6F, 4));
          other.setMonome(new Monome(33F, 3));
          assertEquals("-256.0xe8 + 154.0xe4 - 33.0xe3 - 32.0xe2 + 1.0",
                  p.difference(other).toString());
    }
}
