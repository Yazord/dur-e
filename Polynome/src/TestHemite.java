import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestHemite {
	
	 private Polynome[] hermite;
	 
	@Before
	public void setUp() {
	        this.hermite = new Polynome[10];
	        this.hermite[0] = new Polynome();
	        this.hermite[0].setMonome(new Monome(1, 0));
	        this.hermite[1] = new Polynome();
	        this.hermite[1].setMonome(new Monome(1, 1));
	        Monome m1 = new Monome(1.0F, 1);
	        for (int i = 2; i < 10; i++) {
	            Monome m2 = new Monome(-i + 1, 0);
	            Polynome terme1 = this.hermite[i - 1].produitMonome(m1);
	            Polynome terme2 = this.hermite[i - 2].produitMonome(m2);
	            this.hermite[i] = terme1.somme(terme2);
	        }
	    }

	@After
	public void tearDown() {
		for(int i = 0; i <this.hermite.length;i++) {
			this.hermite[i]=null;
		}
	}
	
	
	@Test
	public void testP1() {
		for (float i=1.0F; i < 9;i++) {
			assertEquals(this.hermite[(int) (i+1)].d�riv�e().toString(),this.hermite[(int) i].produitMonome(new Monome(i+1,0)).toString());
		}
	}
	
	@Test
	public void testP2() {
		for (int i=1; i < 9;i++) {
			assertEquals(this.hermite[i].d�riv�e().d�riv�e().somme(this.hermite[i].d�riv�e().produitMonome(new Monome(-1,1))).somme(this.hermite[i].produitMonome(new Monome(i,0))).toString(),"0");
		}
		
	}
	
}
