import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPoint {
	
	private Point p;
	
	@Before
	public void setUp() {
		this.p = new Point(1F,2F);
	}
	
	
	@After
	public void tearDown() {
		this.p = null;
	}

	@Test
	public void testOrigine() {
		assertEquals(0F, Point.POINT_ORIGINE.getAbscisse(), 0F);
		assertEquals(0F, Point.POINT_ORIGINE.getOrdonnée(), 0F);
	}
	

	@Test
    public void testGetters() {
		 assertEquals(1F, p.getAbscisse(), 0F);
		 assertEquals(2F, p.getOrdonnée(), 0F);
    }

	@Test
	public void testTranslater() {
		p.translater(1, 2);
		assertEquals(2F, p.getAbscisse(),0F);
		assertEquals(4F, p.getOrdonnée(), 0F);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testHNegatif(){
		p.mettreAEchelle(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testHZero(){
		p.mettreAEchelle(0);
	}
	
	@Test
	public void testMettreAEchelle() {
	p.mettreAEchelle(2);
	assertEquals(2F, p.getAbscisse(),0F);
	assertEquals(4F, p.getOrdonnée(), 0F);
	
	}
	
}
