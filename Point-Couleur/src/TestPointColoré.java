import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPointColoré extends TestPoint{
	
	private PointColoré p;
	
	@Before
	public void setUp() {
		super.setUp();
		this.p = new PointColoré(1F, 2F, 3, 4, 5);
		
	}
	
	@After
	public void tearDown() {
		super.tearDown();
		this.p = null;
	
	}
	
	@Test
	public void testOrigineC() {
		assertEquals(0F,PointColoré.POINT_ORIGINE.getAbscisse(),0F);
		assertEquals(0F,PointColoré.POINT_ORIGINE.getOrdonnée(),0F);
		assertEquals(0,PointColoré.POINT_ORIGINE.getNuanceRouge());
		assertEquals(0,PointColoré.POINT_ORIGINE.getNuanceVert());
		assertEquals(0,PointColoré.POINT_ORIGINE.getNuanceBleu());
	}
	
	@Test
	public void testGetNuanceRouge() {
		assertEquals(3,this.p.getNuanceRouge());
	}
	
	@Test
	public void testGetNuanceVert() {
		assertEquals(4,this.p.getNuanceVert());
	}
	
	@Test
	public void testGetNuanceBleu() {
		assertEquals(5,this.p.getNuanceBleu());
	}

	@Test
	public void testSetNuanceRouge() {
		this.p.setNuanceRouge(6);
		assertEquals(6, this.p.getNuanceRouge());
		assertEquals(4, this.p.getNuanceVert());
		assertEquals(5, this.p.getNuanceBleu());
	}
	
	@Test
	public void testSetNuanceVert() {
		this.p.setNuanceVert(6);
		assertEquals(3, this.p.getNuanceRouge());
		assertEquals(6, this.p.getNuanceVert());
		assertEquals(5, this.p.getNuanceBleu());
	}
	
	@Test
	public void testSetNuanceBleu() {
		this.p.setNuanceBleu(6);
		assertEquals(3, this.p.getNuanceRouge());
		assertEquals(4, this.p.getNuanceVert());
		assertEquals(6, this.p.getNuanceBleu());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetRougeN() {
		this.p.setNuanceRouge(-2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetRougeG() {
		this.p.setNuanceRouge(256);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetVerTN() {
		this.p.setNuanceVert(-2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetVertG() {
		this.p.setNuanceVert(256);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetBleuN() {
		this.p.setNuanceBleu(-2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetBleuG() {
		this.p.setNuanceBleu(256);
	}

}
