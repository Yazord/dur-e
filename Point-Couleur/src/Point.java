
public class Point {
	
	private float abscisse;
	private float ordonn�e;
	
	public Point(float x, float y) {
		this.abscisse = x;
		this.ordonn�e = y;
	}
	
	public static final Point POINT_ORIGINE = new Point(0F,0F);
	
	public float getAbscisse() {
		return this.abscisse;
	}
	
	public float getOrdonn�e() {
		return this.ordonn�e;
	}
	
	public void translater(float tx, float ty) {
		this.abscisse += tx;
		this.ordonn�e += ty;
	}
	
	public void mettreAEchelle(float h)throws IllegalArgumentException{
		if (h <= 0) {
			throw new IllegalArgumentException("Erreur de coef pour mettre a l'echelle");
		}
		this.abscisse *= h;
		this.ordonn�e *= h;
	}
	
	@Override
	public String toString() {
		return "("+this.abscisse+";"+this.ordonn�e+")";
	}
}
