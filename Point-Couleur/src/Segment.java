
public class Segment {
	
	private Point origine;
	private Point extr�mit�;
	
	
	public Segment(Point o, Point e) {
		this.origine = o;
		this.extr�mit� = e;
	}
	
	public Point pointMillieu() {
		float xa = this.origine.getAbscisse();
		float xb = this.extr�mit�.getAbscisse();
		float ya = this.origine.getOrdonn�e();
		float yb = this.extr�mit�.getOrdonn�e();
	return new Point((xa+xb) / 2, (ya + yb) / 2);
	}
}
