
public class Couleur {
	
	private int r;
	private int v;
	private int b;
	
	
	public Couleur(int r, int v, int b)throws IllegalArgumentException {
		this.setRouge(r);
		this.setBleu(b);
		this.setVert(v);
	}
	
	
	public static final Couleur ROUGE = new Couleur(255,0,0);
	public static final Couleur BLEU = new Couleur(0,0,255);
	public static final Couleur VERT = new Couleur(0,255,0);
	
	
	public int getRouge() {
		return this.r;
	}
	
	public int getVert() {
		return this.v;
	}
	
	public int getBleu() {
		return this.b;
	}
	
	
	public void setRouge(int r) throws IllegalArgumentException {
		if (r > 255 || r < 0) {
			throw new IllegalArgumentException("Erreur valeur rouge");
		}
		this.r = r;
	}
	
	public void setVert(int v) throws IllegalArgumentException {
		if (v > 255 || v < 0) {
			throw new IllegalArgumentException("Erreur valeur vert");
		}
		this.v = v;
	}
	
	public void setBleu(int b) throws IllegalArgumentException {
		if (b > 255 || b < 0) {
			throw new IllegalArgumentException("Erreur valeur bleu");
		}
		this.b = b;
	}
	
	
	public int getValeurRvb() {
		return this.r * 256 *256 + this.v * 256 + this.b;
	}
	
	@Override
	public String toString() {
		return "["+this.r+","+this.v+","+this.b+"]";
	}
	
}
