import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestCouleur {
	
	
	private Couleur c;
	
	@Before
	public void setUp() {
		this.c = new Couleur(1,2,3);
	}
	
	@After
	public void tearDown() {
		this.c = null;
	}

	@Test
	public void testRouge() {
		assertEquals(255 , Couleur.ROUGE.getRouge());
		assertEquals(0 ,  Couleur.ROUGE.getVert());
		assertEquals(0,  Couleur.ROUGE.getBleu());
	}
	
	
	@Test
	public void testVert() {
		assertEquals(0 ,  Couleur.VERT.getRouge());
		assertEquals(255 , Couleur.VERT.getVert());
		assertEquals(0, Couleur.VERT.getBleu());
	}
	
	@Test
	public void testBleu() {
		assertEquals(0 , Couleur.BLEU.getRouge());
		assertEquals(0 ,  Couleur.BLEU.getVert());
		assertEquals(255,  Couleur.BLEU.getBleu());
	}
	
	@Test
	public void testGetRouge() {
		assertEquals(1, c.getRouge());	
		
	}
	
	@Test
	public void testGetVert() {
		assertEquals(2, c.getVert());	
		
	}
	
	
	@Test
	public void testGetBleu() {
		assertEquals(3, c.getBleu());	
		
	}
	
	
	@Test 
	public void testSetRouge() {
		c.setRouge(4);
		assertEquals(4, c.getRouge());
		assertEquals(2 , c.getVert());
		assertEquals(3 , c.getBleu());
		
	}
	
	@Test
	public void testSetVert() {
		c.setVert(4);
		assertEquals(1 , c.getRouge());
		assertEquals(4 , c.getVert());
		assertEquals(3 , c.getBleu());
	}
	@Test
	public void testSetBleu() {
		c.setBleu(4);
		assertEquals(1 , c.getRouge());
		assertEquals(2 , c.getVert());
		assertEquals(4 , c.getBleu());
	}
	
	@Test
	public void testValeurRVB() {
		int rvb = 1 * 256 * 256 + 2*256 + 3;
		assertEquals(rvb, c.getValeurRvb());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionRougeN() {
		@SuppressWarnings("unused")
		Couleur c = new Couleur(-1,2,3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionVertN() {
		@SuppressWarnings("unused")
		Couleur c = new Couleur(1,-2,3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionBleuN() {
		@SuppressWarnings("unused")
		Couleur c = new Couleur(1,2,-3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionRougeG() {
		@SuppressWarnings("unused")
		Couleur c = new Couleur(256,2,3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionVert() {
		@SuppressWarnings("unused")
		Couleur c = new Couleur(1,256,3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionBleu() {
		@SuppressWarnings("unused")
		Couleur c = new Couleur(1,2,256);
	}
	

}
