import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestMonome {
	
	private Monome m;
	
	@Before
	public void setUp() throws Exception{
		this.m = new Monome(2.0F,4); 
	}
	
	@After
	public void setDown() {
		this.m = null;
	}
	@Test
	public void testGetteur() {
		assertEquals(2.0F,this.m.getCoefficient(),0.0F);
		assertEquals(4, this.m.getExposant());
	}
	
	@Test
	public void testNul() {
		assertTrue(new Monome(0F,0).estNul());
		assertTrue(new Monome(0F,2).estNul());
		assertFalse(this.m.estNul());
		assertFalse(new Monome(2F,0).estNul());
	}
	
	@Test
	public void testSomme() {
		Monome m2 = new Monome(1.0F,4);
		assertEquals(3,this.m.somme(m2).getCoefficient(),0F);
		assertEquals(4,this.m.somme(m2).getExposant());
	}
	
	@Test
	public void testProduit() {
		Monome m2 = new Monome(1.0F,4);
		assertEquals(2,this.m.produit(m2).getCoefficient(),0F);
		assertEquals(8,this.m.produit(m2).getExposant());
	}
	
	@Test 
	public void testDeriveNul() {
		Monome m0 = new Monome(0F,0);
		assertEquals(0,m0.d�riv�e().getCoefficient(),0.0F);
		assertEquals(0,m0.d�riv�e().getExposant());
	}
	
	@Test
	public void testDerive() {
		assertEquals(8,this.m.d�riv�e().getCoefficient(),0.0F);
		assertEquals(3,this.m.d�riv�e().getExposant());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExposantNegatif() {
		this.m = new Monome(1,-2);
	}
	
	@Test(expected = ArithmeticException.class)
	public void testExposantDifferent() {
		this.m = this.m.somme(new Monome(1.0F,2));
	}
	
	@Test
	public void testToString() {
		assertEquals("2.0xe4",this.m.toString());
		assertEquals("0",new Monome(0F,0).toString());
		assertEquals("4.0",new Monome(4F,0).toString());
	}
}
