import java.util.ArrayList;
import java.util.List;

public class Application {

	public static void main(String[] args) {
		List<Figure> f = new ArrayList<Figure>();
		
		
		PolyligneOuverte pl = new PolyligneOuverte(new Point(40,30), new Point(70,80));
		pl.setPoint(1,new Point(200,30));
		pl.setPoint(2,new Point(10,10));
		f.add(pl);
		new FenetreFigures(f);
		for(Figure e : f) {
			System.out.println(e);
		}
	}

}
