import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

public class PolyligneOuverte extends Figure{
	
	private List<Point> l;
	
	public PolyligneOuverte(Point p1, Point p2) throws IllegalArgumentException{
		this.l = new ArrayList<>();
		if(p1.equals(p2)) {
			throw new IllegalArgumentException("2 points identiques");
		}
		this.l.add(p1);
		this.l.add(p2);
		
	}
	
	public int nombre() {
		return this.l.size();
	}
	
	public Point ieme(int i) {
		return this.l.get(i);
	}
	
	public void setPoint(int i,Point p) throws IllegalArgumentException{
		if(i<0 || i>this.nombre()) {
			throw new IllegalArgumentException();
		}
		for(Point pt : this.l) {
			if(p.equals(pt)) {
				throw new IllegalArgumentException();
			}
		}
		this.l.add(i, p);
	}
	
	public void SupprIeme(int i) {
		this.l.remove(i);
	}
	
	public int longueur() {
		int longueur = 0;
		for(int i=0; i<this.l.size()-1;i++) {
			longueur += Point.distance(this.l.get(i), this.l.get(i+1));
		}
		return longueur;
	}
	
	public void tracer(Graphics g) {
		for(int i=0; i<this.l.size()-1;i++) {
			g.drawLine(this.ieme(i).getX(), this.ieme(i).getY(), this.ieme(i+1).getX(), this.ieme(i+1).getY());
		}
	}
}
