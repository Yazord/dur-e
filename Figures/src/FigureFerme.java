import java.awt.Graphics;

public abstract class FigureFerme extends Figure{
	
	private Point p1;
	private Point p2;
	
	public FigureFerme(Point p1, Point p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	
	public Point getPoint1() {
		return this.p1;
	}
	
	public Point getPoint2() {
		return this.p2;
	}
	
	@Override
	public String toString() {
		return "Point 1 : "+this.p1.toString()+" Point 2 : "+this.p2.toString();
	}

	public abstract double surface();
	
	public abstract double perimetre();
	
	public abstract void tracer(Graphics g);
	
	
	
	
}
