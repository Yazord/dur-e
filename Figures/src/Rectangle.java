import java.awt.Graphics;

public class Rectangle extends FigureFerme{

	public Rectangle(Point p1, Point p2) {
		super(p1,p2);
	}
	
	public int largeur() {
		return Math.abs(this.getPoint1().getX()-this.getPoint2().getX());
	}
	
	public int hauteur() {
		return Math.abs(this.getPoint2().getY() - this.getPoint1().getY());
	}

	public double perimetre() {
		return 2*(this.largeur() + this.hauteur());
	}
	
	public double surface() {
		return this.largeur()*this.hauteur();
	}
	
	public void tracer(Graphics g) {
		g.drawRect(this.getPoint1().getX(),this.getPoint1().getY(),this.largeur(), this.hauteur());
	}
	
	@Override
	public String toString() {
		return super.toString()+" Perimetre : "+this.perimetre()+" surface : "+this.surface()+" largeur : "+this.largeur()+" hauteur : "+this.hauteur();
	}

}
