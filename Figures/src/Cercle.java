import java.awt.Graphics;

public class Cercle extends FigureFerme{

	
	public Cercle(Point p1, Point p2) {
		super(p1,p2);
	}
	
	public double rayon() {
		return Point.distance(this.getPoint1(), this.getPoint2());
	}
	
	public double diametre() {
		return this.rayon()*2;
	}
	
	
	public double perimetre() {
		return 2*Math.PI*this.rayon();
	}
	
	public double surface() {
		return Math.PI*Math.pow(this.rayon(), 2);
	}
	
	public void tracer(Graphics g) {
		Point p = new Point((int)(this.getPoint1().getX()-this.rayon()),(int)(this.getPoint1().getY()-this.rayon()));
		g.drawOval(p.getX(), p.getY(), (int)this.diametre(), (int)this.diametre());
	}

	@Override
	public String toString() {
		return super.toString()+" Perimetre : "+this.perimetre()+" surface : "+this.surface()+" Rayon : "+this.rayon()+" Diametre : "+this.diametre();
	}
	
}
